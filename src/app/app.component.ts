import { Component, Input } from '@angular/core';
import { data1 } from './data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  public response: any;
  public len: any;
  public countryArray = [];
  public duplicatieArray = [];
  public show = false;
  public output: any;

  constructor() { }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    this.printList();
  }

  printList() {
    this.response = data1.processed;
    this.len = this.response.length;
    for (let i = 0; i < this.len; i++) {
      const k = this.check(this.countryArray, this.response[i].country_name);
      if (k === 0) {
        this.countryArray.push(this.response[i].country_name);
      }
    }

    for (let i = 0; i < this.len; i++) {
      for (let j = i + 1; j < this.len; j++) {
        if (this.response[i].country_name === this.response[j].country_name && this.response[i].city_name === this.response[j].city_name) {
            this.addObject(j);
        }
      }
    }

    // console.log('Enries need to be deleted' + JSON.stringify(this.duplicatieArray));
    this.show = true;
    this.output = this.duplicatieArray;
  }

  addObject(val) {
    const duplicateEntry = {};
    duplicateEntry['country_name'] = this.response[val].country_name;
    duplicateEntry['city_name'] = this.response[val].city_name;
    duplicateEntry['location_master_id'] = this.response[val].location_master_id;
    this.duplicatieArray.push(duplicateEntry);
  }

  check(arr, name) {
    if (arr.length === 0) {
      return 0;
    } else {
      for (let i = 0; i < arr.length; i++) {
        if (arr[i] === name) {
          return 1;
        }
      }
      return 0;
    }
  }

}
